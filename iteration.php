<?php

  class Posts {
    public $post;
    public $hasPosts = true;

    function __construct($post) {
      $this->post = $post;
    }

    function hello() {
      echo "hello"; 
    }
  } 

$post1 = new Posts("First post");

foreach ($post1 as $key => $value) {
  echo "Key: " . $key . PHP_EOL;
  echo "Value: " . $value . PHP_EOL;
}
 
PHP_EOL;

var_dump($post1);

echo PHP_EOL;
